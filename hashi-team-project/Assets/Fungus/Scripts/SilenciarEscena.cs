﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SilenciarEscena : MonoBehaviour
{
    public AudioMixer audioMixer;
    public GameObject controladorSonido;
    public Sprite volumenActivado;
    public Sprite volumenDesactivado;

    private bool sonido = true;
    private SpriteRenderer estadoSonido;
    // Start is called before the first frame update
    void Start()
    {
        estadoSonido = controladorSonido.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActivarDesactivarSonido()
	{
        if(sonido)
		{
            estadoSonido.sprite = volumenDesactivado;
            audioMixer.SetFloat("MusicVol", -80f);
            sonido = false;
		} else
		{
            audioMixer.SetFloat("MusicVol", 0f);
            estadoSonido.sprite = volumenActivado;
            sonido = true;
		}
	}
}
